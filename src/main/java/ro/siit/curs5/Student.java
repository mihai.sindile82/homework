package ro.siit.curs5;


public class Student extends Person{
    private int studentID;
    private String groupe;
    private String year;

    public int getStudentID() {
        return studentID;
    }

    public void setStudentID(int studentID) {
        this.studentID = studentID;
    }

    public String getGrupa() {
        return groupe;
    }

    public void setGroupe(String groupe) {
        this.groupe = groupe;
    }

    public String getAn() {
        return year;
    }

    public void setAn(String an) {
        this.year = year;
    }



    public Student(String name, String address, String cnp, int age, String gender, String groupe, String year, int studentID) {
        super(name, address, cnp, age, gender);
        this.studentID = studentID;
        this.groupe = groupe;
        this.year = year;
    }

    @Override
    public String toString()
    {
        return super.toString() +
                ", groupe='" + groupe + '\'' +
                ", year='" + year + '\'' +
                ", studentID=" + studentID;
    }
}
