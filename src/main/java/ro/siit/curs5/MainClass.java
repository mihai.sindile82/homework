package ro.siit.curs5;

import ro.siit.curs4.Shape;
//import ro.siit.curs4.Square;
import ro.siit.curs4.Triangle;

import java.awt.*;

public class MainClass {

    public static void main(String[] args) {
        Car logan = new Car(Color.GREEN, (byte) 5, 150);
        logan.start();
        System.out.println(logan.toString());
        logan.accelerate();
        logan.accelerate();
        logan.accelerate();
        System.out.println(logan.toString());
        Car logan2 = new Car(Color.red, (byte) 5, 150);
        System.out.println(logan.toString());
        //Car.setNrOfCars(6);
        System.out.println(logan2.toString());
        Car.horn();
        //car in the 5th gear
        Car logan3 = new Car(Color.BLUE, (byte) 5, 150);
        logan3.start();
        System.out.println(logan3.toString());
        logan3.accelerate();
        logan3.accelerate();
        logan3.accelerate();
        logan3.accelerate();
        logan3.accelerate();
        logan3.accelerate();
        logan3.accelerate();
        logan3.accelerate();
        logan3.accelerate();
        logan3.accelerate();
        logan3.accelerate();
        logan3.accelerate();
        logan3.accelerate();
        System.out.println(logan3.toString());


        //inheritance from here on down

        Triangle tr = new Triangle(Color.black);
        tr.draw();
        tr.erase();

        System.out.println("Verify polymorphism...");
        Shape shapeTr = new Triangle(Color.green);
        Shape shape = new Shape(Color.red);
        //Shape shapeSquare = new Square(5,Color.red);

        shapeTr.draw();
        shapeTr.erase();
        shape.draw();
        shape.erase();

        //Person student teacher exercise bellow


        System.out.println("----------------Inheritance Exercise-----------------");
        Person person1 = new Person("Ion Vasile", "Bucuresti", "12345678910", 45, "M");
        Teacher teacher1 = new Teacher("Popescu Vasilica", "Bucuresti", "88888888888", 60, "F", "Math", 1212);
        Student student1 = new Student("Sindile Mihai", "Bucuresti", "1111111111111", 38, "M", "D", "3", 2099);
        System.out.println(person1);//if toString exists in class, it will be called by default
        System.out.println(teacher1.toString());
        System.out.println(student1.toString());

    }

}
