package ro.siit.curs6;

import java.util.ArrayList;
import java.util.List;

public class MainHomework {

    public static void main(String[] args) {

        Building building;
        List<Floor> floors = new ArrayList<>();

        Floor firstFloor = new Floor();
        //alternative without the addRooms we made in Floor Class
//        firstFloor.addRoom(new OfficeRoom());
//        firstFloor.addRoom(new ToiletteRoom());
//        firstFloor.addRoom(new ToiletteRoom());
//        firstFloor.addRoom(new ConferenceRoom());
//        firstFloor.addRoom(new ConferenceRoom());
//        firstFloor.addRoom(new ConferenceRoom());
        firstFloor.addRooms(RoomType.OFFICE_SPACE, 1);
        firstFloor.addRooms(RoomType.TOILETS, 2);
        firstFloor.addRooms(RoomType.KITCHEN, 1);
        firstFloor.addRooms(RoomType.CONFERENCE_ROOMS,3);
        floors.add(firstFloor);

        Floor secondFloor = new Floor();
        secondFloor.addRooms(RoomType.OFFICE_SPACE, 2);
        secondFloor.addRooms(RoomType.TOILETS, 2);
        secondFloor.addRooms(RoomType.KITCHEN, 1);
        secondFloor.addRooms(RoomType.CONFERENCE_ROOMS, 4);
        floors.add(secondFloor);

        Floor thirdFloor = new Floor();
        thirdFloor.addRooms(RoomType.TOILETS, 2);
        thirdFloor.addRooms(RoomType.CONFERENCE_ROOMS, 6);
        floors.add(thirdFloor);

        building = new Building(floors);
        System.out.println(building);

    }

}
