package ro.siit.curs6;

import ro.siit.curs4.Triangle;

import java.awt.*;
import java.util.*;
import java.util.List;

public class CollectionsEx {

    public static void showArrays(){
        int [] arrays = {1,3,4,5};
        System.out.println("Elment poz. 3: " + arrays[2]);

        Car [] Cars = {new Truck(), new Truck()};

        //display entire array

        for (int i = 0; i <= arrays.length-1; i++){
            System.out.println(arrays[i]);
        }

        //display with forEch

        for (int el: arrays){
            System.out.println(el);
        }

        String [] names = {"Ion", "Maria", "Vasile"};
        for (String ele: names){
            System.out.println(ele);
        }

        int[] numbers = new int[100];
    }

    public static void displayList(List list){
        for (Object lst: list){
            System.out.println(lst.toString());
        }

    }

    public static void showList(){
        List arrayList = new ArrayList();
        arrayList.add(1);
        arrayList.add("Ana");
        arrayList.add("Ana are mere");
        arrayList.add(1,2);

        displayList(arrayList);
        arrayList.remove("Ana");
        displayList(arrayList);

        List<String> names = new ArrayList<String>();
        names.add("Ion");
        names.add("Vasile");
        names.add("Vasile");
        names.add("Ana");
        System.out.println("Size of names: " + names.size() + "->" + names.toString());
        System.out.println(names.get(1));
        //sort the list
        Collections.sort(names);
        System.out.println(names);

    }

    //set - list with unique elements
    public static void showSet(){
        Set<Integer> setList = new HashSet<Integer>();
        setList.add(1);
        setList.add(2);
        setList.add(3);

        System.out.println(setList.toString());
    }

    public static void showHashMap(){
        Map<String, Integer> hashMap = new HashMap<String,Integer>();
        hashMap.put("Laptop", 3);
        hashMap.put("PC", 5);
        hashMap.put("PC", 2);
        hashMap.put("Mobile", 4);

        //display
        displayMap(hashMap);
    }

    public static void displayMap(Map hashMap){
        List keys = new ArrayList(hashMap.keySet());
        Collections.sort(keys);
        for (Object key : keys) {
            System.out.println(key + "->" + hashMap.get(key));
        }
    }

    public static void main(String[] args) {
        showArrays();
        showList();
        showSet();
        showHashMap();
    }
}
