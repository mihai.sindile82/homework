package ro.siit.curs6;

public enum RoomType {
    OFFICE_SPACE("Office Spaces"),
    TOILETS("Toilets"),
    KITCHEN("Kitchen"),
    CONFERENCE_ROOMS("Conference Rooms");

    RoomType(String label) {
        this.label = label;
    }

    public final String label;

    @Override
    public String toString() {
        return label;
    }

}
