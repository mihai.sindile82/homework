package ro.siit.curs6;

import java.util.HashMap;

public class Floor {

//    public Floor(HashMap<RoomType, Integer> rooms) {
//        this.rooms = rooms;
//    }

    private HashMap<RoomType, Integer> rooms = new HashMap<>();

    public Floor() {

    }

    public void addRoom(Room room){
        Integer counter = this.rooms.get(room.getRoomType());
        if (counter == null){
            counter = 0;
        }
        this.rooms.put(room.getRoomType(), counter+1);
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (RoomType key : this.rooms.keySet()) {
            result.append("        ");
            result.append(this.rooms.get(key));
            result.append(" ");
            result.append(key);
            result.append("\n");
        }
        return result.toString();
    }

    public void addRooms(RoomType roomType, int numberOfRooms){
        for (int i=0; i< numberOfRooms; i++) {
            Room room;
            //factory pattern needed
            switch (roomType) {
                case CONFERENCE_ROOMS:
                    room = new ConferenceRoom();
                    break;
                case KITCHEN:
                    room = new KitchenRoom();
                    break;
                case TOILETS:
                    room = new ToiletteRoom();
                    break;
                case OFFICE_SPACE:
                    room = new OfficeRoom();
                    break;
                default:
                    return;
            }
            this.addRoom(room);
        }
    }
}
