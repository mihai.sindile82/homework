package ro.siit.curs6;

import java.util.*;

import static ro.siit.curs6.StringFormatFirstAttempt.removeUndesired;

public class MainBuildingFirstAttempt {

    public static void showRooms(){
        //We will use a Hashmap of Hashmaps :D so that the number of floors won't be hardcoded, but instead the number of added hashmap floors
        HashMap<HashMap<Integer, String>, Integer> first = new HashMap<HashMap<Integer, String>, Integer>();

        //Init first floor
        HashMap<Integer, String> firstFloor = new HashMap<Integer, String>();
        firstFloor.put(2, "Toilets");
        firstFloor.put(1, "Kitchen");
        firstFloor.put(3, "Conference Rooms");

        //Init second floor
        HashMap<Integer, String> secondFloor = new HashMap<Integer, String>();
        secondFloor.put(2, "Office Space");
        secondFloor.put(2, "Toilets");
        secondFloor.put(1, "Kitchen");
        secondFloor.put(4, "Conference Rooms");

        //Init third floor
        HashMap<Integer, String> thirdFloor = new HashMap<Integer, String>();
        thirdFloor.put(2, "Toilets");
        thirdFloor.put(6, "Conference Rooms");

        first.put(firstFloor, 1);
        first.put(secondFloor, 2);
        first.put(thirdFloor, 3);
        displayFloors(first);
        displayRooms(first);
    }

    //number of floors. Yes, I played a little, for learning purposes :)
    public static void displayFloors(Map hashMap){
        int i;

        List floors = new ArrayList(hashMap.keySet());
        i=0;
        //how many floors counter
        for (Object key : floors) {
            i++;
        }
        System.out.println("The building has " + i +" floors");
    }

    public static void displayRooms(Map hashMap){
        List floors = new ArrayList(hashMap.keySet());
        //reverse the collections
        Collections.reverse(floors);
        //detail each floor
        for (Object key : floors) {
            System.out.println("Floor " + hashMap.get(key) +": ");
            System.out.println(removeUndesired(key.toString()));
        }
    }

    public static void main(String[] args) {

        showRooms();

    }
}

