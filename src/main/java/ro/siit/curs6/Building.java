package ro.siit.curs6;

import com.sun.org.apache.xpath.internal.functions.FuncFloor;

import java.util.List;

public class Building {
    private List<Floor> floors;

    public Building(List<Floor> floors) {
        this.floors = floors;
    }

    @Override
    public String toString(){
        if (this.floors == null){
            return ("");
        }

        StringBuilder result = new StringBuilder();
        result.append("The building has ");
        result.append("this.floors.size()");
        result.append(" floors.\n");

        for (int i = 0; i < this.floors.size(); i++){
            Floor floor = this.floors.get(i);
            if (floor == null){
                continue;
            }
            result.append("Floor ");
            result.append(i+1);
            result.append(": \n");
            result.append(floor.toString());
            result.append("\n");
        }

        return result.toString();
    }
}
