package ro.siit.curs6;

public abstract class Room
{

    protected RoomType roomType;

    public RoomType getRoomType()
    {
        return this.roomType;
    }

}