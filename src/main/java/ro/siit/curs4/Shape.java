package ro.siit.curs4;

import java.awt.*;

public class Shape {
    private Color color;

    public Shape(Color color) {
        this.color = color;
    }

    public Shape() {

    }

    public String getColorName() {

        if(this.color.equals(Color.red))
            return "RED";
        else if(this.color.equals(Color.green))
            return "GREEN";
        else if(this.color.equals(Color.blue))
            return "BLUE";
        else if(this.color.equals(Color.yellow))
            return "YELLOW";
        else if(this.color.equals(Color.black))
            return "BLACK";
        else if(this.color.equals(Color.white))
            return "WHITE";
        else
            return "Unknown Color";
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void draw() {
        System.out.println("Draw a shape with color:" + getColorName());
    }

    public void erase() {
        System.out.println("Erase the shape with color:" + getColorName());
    }
}
