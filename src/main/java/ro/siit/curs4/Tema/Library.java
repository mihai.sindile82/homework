package ro.siit.curs4.Tema;

public class Library {

    public static void main(String[] args) {

    Author a1  = new Author("Isaac Asimov", "dead@longtime.ago");


    Book b1 = new Book("Foundation", 1942,a1,25);

        System.out.println("Book " + b1.name + " (" + b1.price + "RON), " + "by " + a1.name + ", published in " + b1.year);

    }

}
