package ro.siit.curs4;

import java.awt.*;
public class Square extends Shape{

    double squareSide;

    public Square() //acesta este un constructor implicit care se apeleaza de cate ori instantiem cu new. El poate sa nu existe scris de noi, Java il creaza automat
    {
        super( Color.black);
    }

    public Square(double squareSide, Color color){
        super(color);
        this.squareSide = squareSide;
    }

    public Square(double parseDouble) {
        super();
    }

    public void setSide(double squareSide){
        this.squareSide = squareSide; //this. se refera la argumentul de sus, nu al functiei. altfel se face confuzie
    }

    protected double getArea(){
        //return  squareSide * squareSide; //aici functia nu are parametru cu acelasi nume, deci nu e nevoie de .this
        return Math.pow(squareSide, 2); //squareside la patrat
    }

    void printSquare(){
        System.out.println("Avem un patrat cu latura " + squareSide + " si cu aria " + getArea());
    }
}
