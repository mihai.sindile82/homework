package ro.siit.curs7;

public class MainAppClass {

    public static void main(String[] args) {
//        throwMyException();
//        System.out.println("Un mesaj");
        try {
            System.out.println(circleArea(-5));
        }

        catch (IllegalArgumentException | ArithmeticException e)  //multiple exceptions
        {
            System.out.println("Argument exception");
        }

        catch (IndexOutOfBoundsException e)
        {
            System.out.println("Index exception");
        }
        catch (Exception e) {    //at the end, if we want to catch all other exceptions not specified above
            System.out.println("Other exceptions");
        }

        finally {
            System.out.println("This block will always run");
        }

//        System.out.println(circleArea(-5));
        try {
            int age = Integer.parseInt(args[0]);
        }

        catch (ArrayIndexOutOfBoundsException e){
            System.out.println("There are no arguments!!");
        }
        try {
            newThrowException();
        }
        catch (MyCustomException e)
        {
            System.out.println(e.getMessage());
        }
    }

    private static void throwMyException(){

        throw new IllegalArgumentException("Aici am aruncat exceptia pentru ca ....");
    }

    private static double circleArea(double radius) throws IllegalArgumentException{
        if (radius < 0) {
            throw new IllegalArgumentException("Radius must be positive value, your value is " + radius);
        }
        return Math.PI * radius * radius;
    }

    private static void newThrowException() throws MyCustomException{

        throw new MyCustomException("S-a intamplat ceva ", 300);
    }

}
