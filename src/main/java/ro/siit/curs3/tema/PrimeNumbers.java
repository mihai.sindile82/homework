package ro.siit.curs3.tema;

public class PrimeNumbers {

    //function to check if number is prime
    public static boolean isPrime(int n) {
        //we must consider all options
        if (n <= 1)
            return false;
        //for the rest of numbers
        for (int i = 2; i < n; i++)
            if (n % i == 0)
                //has divisors
                return false;
        //if it's prime
        return true;
    }

    //function to show prime numbers
    public static void showPrime(int n) {
        for (int i = 2; i <= n; i++) {
            //if our function returns true, print the prime number
            if (isPrime(i))
                System.out.println(i + " is prime");
        }
    }

    public static void main(String[] args) {
        //here we set our number as per homework and call the show function
        int n = 1000000;
        //we could have called the function directly with 1000000 instead of n
        showPrime(n);
    }
}
