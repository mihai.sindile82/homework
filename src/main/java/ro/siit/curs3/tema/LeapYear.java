package ro.siit.curs3.tema;

public class LeapYear {

    public static void main(String[] args) {

        //Here i initialised the 2 variables that represent the number of days, so these can be changed if needed
        int fLeap = 29;
        int fNotLeap = 28;
        //Argument as year input
        int year = Integer.parseInt(args[0]);
        //conditions for leap year
        boolean isLeapYear = ((year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0));

        //condition for showing days in february accordingly
            if (isLeapYear) {
                System.out.println("The year " + year + " is a leap year, so february has " + fLeap + " days");
                }
            else
            {
                System.out.println("The year " + year + " is not a lap year, so february has " + fNotLeap + " days");
            }

    }
}