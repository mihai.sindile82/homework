package ro.siit.curs3.tema;

public class SumOfNumbers {

    public static void main(String[] args) {
        int sum = 0;

        for(int i = 1; i <= 100; ++i) {
            sum += i;
        }

        System.out.println("The Sum of first 100 numbers is: " + sum);
    }
}