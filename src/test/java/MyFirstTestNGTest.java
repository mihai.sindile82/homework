import org.junit.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import ro.siit.curs7.Calculator;

@Test
public class MyFirstTestNGTest {
    Calculator c;

    @BeforeClass(groups = {"smoke"})
    public void setUp() {
        c = new Calculator();
    }

    @Test(description = "This is first testNG test", priority = 1, groups = {"smoke"})
    public void myTestMethod1() {
        System.out.println("This is a test method 1");
    }

    public void myTestMethod2() {
        System.out.println("This is a test method 2");
    }

    private void myPrivateMethod() {  //nu e un test, ci metoda ajutatoare pt alte teste
        System.out.println("This is NOT a test method");
    }

    @Test(description = "This is test methode to show priority")
    public void myTestMethod3() {
        System.out.println("This is a test method 3");
        this.myPrivateMethod(); //folosim ca element ajutator pentru clasa actuala de test
    }

    @Test(groups = {"smoke", "regression"})
    public void testSum03() {
        Assert.assertEquals(1000, c.compute(1000, 0, "+"), 0);
    }

    @Test(dependsOnMethods = {"testSum03"}, groups = {"smoke", "regression"})
    public void testDependsOnMethod() {
        System.out.println("---->Depends on method run");
    }

    @Test(dependsOnMethods = {"testSum03"}, alwaysRun = true, groups = {"smoke", "regression"})
    public void testDependsOnMethodAlwaysRun() {
        System.out.println("---->Always Run depends on method run");
    }

    @Test(expectedExceptions = {IllegalArgumentException.class}, groups = {"smoke", "regression"})
    public void testException() {
        Assert.assertEquals(1001, c.compute(1000, 0, "x"), 0);
    }

    @Test(groups = {"smoke"})
    @Parameters({"email", "password"})
    public void testLogin(String email, String password) {
        System.out.println("Login with email: " + email + " and password: " + password);
    }

    @Test
    public void getParamsFromCmd() {
        String browser = System.getProperty("browser");
        System.out.println("Run this test with browser: " + browser);
    }

    @DataProvider(name="registrationData")
    public Object[][] registerDataProvider(){
        return new Object[][]{
                {"test@test.com","Ion","Vasile", "Str. Ion Creanga"},
                {"maria@test.com","Maria","Popescu", "Str. Turda"},
                {"vasilica@test.com","Vasilica","Ionescu", "Str. No name"}
        };
    }


    @Test(dataProvider = "registrationData")
    public void registrationTest(String email, String prenume, String nume, String adresa){
        System.out.println("Register with email: " + email + "\nnume si prenume: " + nume + " " + prenume + "\nadresa: " + adresa);
    }

    @DataProvider(name="registrationData2")
    public Object[][] registerDataProvider2(){
        return new Object[][]{
                {"test@test.com","Ion","Vasile"},
                {"maria@test.com","Maria","Popescu"},
                {"vasilica@test.com","Vasilica","Ionescu"}
        };
    }

    @Test(dataProvider = "registrationData2")
    public void registrationTest2(String email, String prenume, String nume){
        System.out.println("Register with email: " + email + "\nnume si prenume: " + nume + " " + prenume);
    }

    @DataProvider(name="registrationData3")
    public Object[][] calculatorTestDataProvider(){
        return new Object[][]{
                {2.0,3.0,"+", 5.0},
                {10.0,10.0,"*", 100.0},
                {9.0,3.0,"/", 3.0}
        };
    }

    @Test(dataProvider = "registrationData3")
    public void testCalculatorWithDP(Double d1, Double d2, String operator, Double expectedResults){
        Assert.assertEquals(expectedResults, c.compute(d1, d2, operator), 0);
    }

}
