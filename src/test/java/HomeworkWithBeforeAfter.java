import io.github.bonigarcia.wdm.WebDriverManager;
import io.github.bonigarcia.wdm.config.DriverManagerType;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class HomeworkWithBeforeAfter {


    @BeforeTest
    public WebDriver initialiseDriverSignUp() {
        WebDriverManager.getInstance(DriverManagerType.CHROME).setup();
        return new ChromeDriver();
    }

    @DataProvider(name = "signupdp")
    public Iterator<Object[]> signupDp(){
        Collection<Object[]> dp = new ArrayList<Object[]>();
        //firstname, lastname, email, username, password, confirmpassword, firstname error, lastname error, email error, username error, password error, confirmpassword error, general error
        dp.add(new String[] {"", "", "", "", "", "",
                "Invalid input. Please enter between 2 and 35 letters",
                "Invalid input. Please enter between 2 and 35 letters",
                "Invalid input. Please enter a valid email address",
                "Invalid input. Please enter between 4 and 35 letters or numbers",
                "Invalid input. Please use a minimum of 8 characters",
                "Please confirm your password"});
        dp.add(new String[] {"Vincent", "", "", "", "", "",
                "",
                "Invalid input. Please enter between 2 and 35 letters",
                "Invalid input. Please enter a valid email address",
                "Invalid input. Please enter between 4 and 35 letters or numbers",
                "Invalid input. Please use a minimum of 8 characters",
                "Please confirm your password"});
        dp.add(new String[] {"Vincent", "Doe", "", "", "", "",
                "",
                "",
                "Invalid input. Please enter a valid email address",
                "Invalid input. Please enter between 4 and 35 letters or numbers",
                "Invalid input. Please use a minimum of 8 characters",
                "Please confirm your password"});
        dp.add(new String[] {"Vincent", "Doe", "doe@doe.com", "", "", "",
                "",
                "",
                "",
                "Invalid input. Please enter between 4 and 35 letters or numbers",
                "Invalid input. Please use a minimum of 8 characters",
                "Please confirm your password"});
        dp.add(new String[] {"Vincent", "Doe", "doe@doe.com", "user001", "", "",
                "",
                "",
                "",
                "",
                "Invalid input. Please use a minimum of 8 characters",
                "Please confirm your password"});
        dp.add(new String[] {"Vincent", "Doe", "doe@doe.com", "user001", "pass2222", "",
                "",
                "",
                "",
                "",
                "",
                "Please confirm your password"});
        dp.add(new String[] {"Vincent", "Doe", "doe@doe.com", "user001", "pass2222", "pass3333",
                "",
                "",
                "",
                "",
                "",
                "Please confirm your password"});


        return dp.iterator();
    }

    @Test(dataProvider = "signupdp")
    public void negativeSubmitTest(String firstname, String lastname, String email, String username, String password, String confirmPassword, String firstNameErrMsg, String lastNameErrMsg, String emailErrMsg, String usernameErrMsg, String passErrMsg, String passConfirmErrMsg){

        initialiseDriverSignUp().get("http://86.121.249.149:4999/stubs/auth.html");
        WebDriverWait wait = new WebDriverWait(initialiseDriverSignUp(), 15);
        WebElement ele = wait.until(
                ExpectedConditions.elementToBeClickable(By.id("register-submit"))
        );
        ele.click();

        WebElement firstNameInput = initialiseDriverSignUp().findElement(By.id("inputFirstName"));
        WebElement lastNameInput = initialiseDriverSignUp().findElement(By.id("inputLastName"));
        WebElement emailInput = initialiseDriverSignUp().findElement(By.id("inputEmail"));
        WebElement usernameInput = initialiseDriverSignUp().findElement(By.id("inputUsername"));
        WebElement passwordInput = initialiseDriverSignUp().findElement(By.id("inputPassword"));
        WebElement passwordConfirmInput = initialiseDriverSignUp().findElement(By.id("inputPassword2"));
        WebElement submitButton = initialiseDriverSignUp().findElement(By.id("register-submit"));


        firstNameInput.clear();
        firstNameInput.sendKeys(firstname);
        lastNameInput.clear();
        lastNameInput.sendKeys(lastname);
        emailInput.clear();
        emailInput.sendKeys(email);
        usernameInput.clear();
        usernameInput.sendKeys(username);
        passwordInput.clear();
        passwordInput.sendKeys(password);
        passwordConfirmInput.clear();
        passwordConfirmInput.sendKeys(confirmPassword);

        submitButton.submit();

        //the percentage with xpath missing the fields is greater than with CSS selectors, so i chose CSS selectors
//        WebElement err1 = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[3]/div/div[2]"));
//        WebElement err2 = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[4]/div/div[2]"));
//        WebElement err3 = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[5]/div[1]/div[2]"));
//        WebElement err4 = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[6]/div/div[2]"));
//        WebElement err5 = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[7]/div/div[2]"));
//        WebElement err6 = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[8]/div/div[2]"));
        WebElement err1 = initialiseDriverSignUp().findElement(By.cssSelector("#registration_form > div:nth-child(3) > div > div.invalid-feedback"));
        WebElement err2 = initialiseDriverSignUp().findElement(By.cssSelector("#registration_form > div:nth-child(4) > div > div.invalid-feedback"));
        WebElement err3 = initialiseDriverSignUp().findElement(By.cssSelector("#registration_form > div:nth-child(5) > div.input-group > div.invalid-feedback"));
        WebElement err4 = initialiseDriverSignUp().findElement(By.cssSelector("#registration_form > div:nth-child(6) > div > div.invalid-feedback"));
        WebElement err5 = initialiseDriverSignUp().findElement(By.cssSelector("#registration_form > div:nth-child(7) > div > div.invalid-feedback"));
        WebElement err6 = initialiseDriverSignUp().findElement(By.cssSelector("#registration_form > div:nth-child(8) > div > div.invalid-feedback"));

        Assert.assertEquals(err1.getText(), firstNameErrMsg);
        Assert.assertEquals(err2.getText(), lastNameErrMsg);
        Assert.assertEquals(err3.getText(), emailErrMsg);
        Assert.assertEquals(err4.getText(), usernameErrMsg);
        Assert.assertEquals(err5.getText(), passErrMsg);
        Assert.assertEquals(err6.getText(), passConfirmErrMsg);
        initialiseDriverSignUp().close();
    }

    @AfterClass
    public void cleanUp() {
        System.out.println("Close driver at end of class test");
        initialiseDriverSignUp().quit();
    }

}
