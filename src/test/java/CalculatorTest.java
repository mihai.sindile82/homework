import org.junit.*;
import ro.siit.curs7.Calculator;

import java.sql.SQLOutput;

public class CalculatorTest {

    static Calculator c;

    @Before
    public void beforeAll(){
        System.out.println("Begin test");
    }

    @After
    public void afterAll(){
        System.out.println("Test complete");
    }

    @BeforeClass
    public static void beforeClass(){
        System.out.println("Begin test suit");
        System.out.println("...............");
        c = new Calculator();
    }

    @AfterClass
    public static void afterClass(){
        System.out.println("................");
        System.out.println("End of test suit");
    }

    @Test
    public void testSum01(){
        //System.out.println(c.compute(2, 7, "+"));
        Assert.assertEquals(9,c.compute(2,7, "+"),0);
    }

    @Test
    public void testSum02(){
        Assert.assertEquals(1110,c.compute(2222,-1112, "+"),0);
    }

    @Test
    public void testSum03(){
        Assert.assertEquals(1000,c.compute(1000,0, "+"),0);
    }

    @Test
    public void testSum04(){
        Assert.assertEquals(-2000,c.compute(-1000,-1000, "+"),0);
    }

    @Test
    public void testSum05(){
        Assert.assertEquals(0,c.compute(-1000,1000, "+"),0);
    }

    @Test
    public void testDiff01(){
        Assert.assertEquals(3334,c.compute(2222,-1112, "-"),0);
    }

    @Test
    public void testDiff02(){
        Assert.assertEquals(2222,c.compute(2222,0, "-"),0);
    }

    @Test
    public void testDiff03(){
        Assert.assertEquals(-1111,c.compute(-2222,-1111, "-"),0);
    }

    @Test
    public void testDiff04(){
        Assert.assertEquals(-3333,c.compute(-2222,1111, "-"),0);
    }

    @Test
    public void testDiff05(){
        Assert.assertEquals(-2222,c.compute(-2222,0, "-"),0);
    }

    @Test
    public void testProduct01(){
        Assert.assertEquals(2222,c.compute(2222,1, "*"),0);
    }

    @Test
    public void testProduct02(){
        Assert.assertEquals(0,c.compute(2222,0, "*"),0);
    }

    @Test
    public void testProduct03(){
        Assert.assertEquals(0,c.compute(0,0, "*"),0);
    }

    @Test
    public void testProduct04(){
        Assert.assertEquals(44,c.compute(-22,-2, "*"),0);
    }

    @Test
    public void testProduct05(){
        Assert.assertEquals(-44,c.compute(-22,2, "*"),0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUnsupp01(){
        Assert.assertEquals(0,c.compute(2222,0, "x"),0);
    }

    @Test
    public void testDiv01(){
        Assert.assertEquals(1111,c.compute(2222,2, "/"),0);
    }

    @Test
    public void testDiv02(){
        Assert.assertEquals(3.33,c.compute(10,3, "/"),0.01);
    }

    @Test
    public void testDiv03(){
        Assert.assertEquals(-3.33,c.compute(-10,3, "/"),0.01);
    }

    @Test
    public void testDiv04(){
        Assert.assertEquals(3.33,c.compute(-10,-3, "/"),0.01);
    }

    //test correctly if an exception is expected to be raised
    @Test(expected = IllegalArgumentException.class)
    public void testDiv05(){
        Assert.assertEquals(3.33,c.compute(10,0, "/"),0);
    }

    //nu e indicat sa prindem exceptia, dar testul va fi passed. Noi trebuie sa tratam exceptia verificand ca ea exista, ca mai sus (testDiv03).
    //please do not use this. for learning purposes only.
//    @Test
//    public void testDiv04(){
//        try {
//            Assert.assertEquals(3.33, c.compute(10, 0, "/"), 0);
//        }
//        catch (IllegalArgumentException e){
//            System.out.println(e.getMessage());
//        }
//    }

    @Test
    public void testSqrt01(){
        Assert.assertEquals(1.4142,c.compute(2,0, "SQRT"),0.001);
    }

    @Test(expected = AssertionError.class)
    public void testSqrt02(){
        Assert.assertEquals(2,c.compute(-4, 0, "SQRT"),0);
    }

    @Test
    public void testSqrt03(){
        Assert.assertEquals(2,c.compute(4, 0, "SQRT"),0);
    }

    @Test
    public void testSqrt04(){
        Assert.assertEquals(0,c.compute(0, 0, "SQRT"),0);
    }

    @Test
    public void testSqrt05(){
        Assert.assertEquals(1000,c.compute(1000000, 0, "SQRT"),0);
    }



}
