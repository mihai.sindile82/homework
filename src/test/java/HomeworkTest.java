import io.github.bonigarcia.wdm.WebDriverManager;
import io.github.bonigarcia.wdm.config.DriverManagerType;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.openqa.selenium.Keys;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class HomeworkTest {

    @DataProvider(name = "signupdp")
    public Iterator<Object[]> signupDp(){
        Collection<Object[]> dp = new ArrayList<Object[]>();
        //firstname, lastname, email, username, password, confirmpassword, firstname error, lastname error, email error, username error, password error, confirmpassword error, general error
        dp.add(new String[] {"", "", "", "", "", "",
                "Invalid input. Please enter between 2 and 35 letters",
                "Invalid input. Please enter between 2 and 35 letters",
                "Invalid input. Please enter a valid email address",
                "Invalid input. Please enter between 4 and 35 letters or numbers",
                "Invalid input. Please use a minimum of 8 characters",
                "Please confirm your password"});
        dp.add(new String[] {"Vincent", "", "", "", "", "",
                "",
                "Invalid input. Please enter between 2 and 35 letters",
                "Invalid input. Please enter a valid email address",
                "Invalid input. Please enter between 4 and 35 letters or numbers",
                "Invalid input. Please use a minimum of 8 characters",
                "Please confirm your password"});
        dp.add(new String[] {"Vincent", "Doe", "", "", "", "",
                "",
                "",
                "Invalid input. Please enter a valid email address",
                "Invalid input. Please enter between 4 and 35 letters or numbers",
                "Invalid input. Please use a minimum of 8 characters",
                "Please confirm your password"});
        dp.add(new String[] {"Vincent", "Doe", "doe@doe.com", "", "", "",
                "",
                "",
                "",
                "Invalid input. Please enter between 4 and 35 letters or numbers",
                "Invalid input. Please use a minimum of 8 characters",
                "Please confirm your password"});
        dp.add(new String[] {"Vincent", "Doe", "doe@doe.com", "user001", "", "",
                "",
                "",
                "",
                "",
                "Invalid input. Please use a minimum of 8 characters",
                "Please confirm your password"});
        dp.add(new String[] {"Vincent", "Doe", "doe@doe.com", "user001", "pass2222", "",
                "",
                "",
                "",
                "",
                "",
                "Please confirm your password"});
        dp.add(new String[] {"Vincent", "Doe", "doe@doe.com", "user001", "pass2222", "pass3333",
                "",
                "",
                "",
                "",
                "",
                "Please confirm your password"});


        return dp.iterator();
    }

    @Test(dataProvider = "signupdp")
    public void negativeSubmitTest(String firstname, String lastname, String email, String username, String password, String confirmPassword, String firstNameErrMsg, String lastNameErrMsg, String emailErrMsg, String usernameErrMsg, String passErrMsg, String passConfirmErrMsg){
        WebDriverManager.getInstance(DriverManagerType.CHROME).setup();
        WebDriver driver = new ChromeDriver();
        driver.get("http://86.121.249.149:4999/stubs/auth.html");

        WebDriverWait wait = new WebDriverWait(driver, 15);
        WebElement ele = wait.until(
                ExpectedConditions.elementToBeClickable(By.id("register-tab"))
        );
        ele.click();


        WebElement firstNameInput = driver.findElement(By.id("inputFirstName"));
        WebElement lastNameInput = driver.findElement(By.id("inputLastName"));
        WebElement emailInput = driver.findElement(By.id("inputEmail"));
        WebElement usernameInput = driver.findElement(By.id("inputUsername"));
        WebElement passwordInput = driver.findElement(By.id("inputPassword"));
        WebElement passwordConfirmInput = driver.findElement(By.id("inputPassword2"));
        WebElement submitButton = driver.findElement(By.id("register-submit"));


        firstNameInput.clear();
        firstNameInput.sendKeys(firstname);
        lastNameInput.clear();
        lastNameInput.sendKeys(lastname);
        emailInput.clear();
        emailInput.sendKeys(email);
        usernameInput.clear();
        usernameInput.sendKeys(username);
        passwordInput.clear();
        passwordInput.sendKeys(password);
        passwordConfirmInput.clear();
        passwordConfirmInput.sendKeys(confirmPassword);
        WebElement sub = wait.until(
                ExpectedConditions.elementToBeClickable(By.id("register-submit"))
        );
        sub.submit();

        WebElement err1 = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[3]/div/div[2]"));
        WebElement err2 = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[4]/div/div[2]"));
        WebElement err3 = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[5]/div[1]/div[2]"));
        WebElement err4 = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[6]/div/div[2]"));
        WebElement err5 = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[7]/div/div[2]"));
        WebElement err6 = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[8]/div/div[2]"));

        Assert.assertEquals(err1.getText(), firstNameErrMsg);
        Assert.assertEquals(err2.getText(), lastNameErrMsg);
        Assert.assertEquals(err3.getText(), emailErrMsg);
        Assert.assertEquals(err4.getText(), usernameErrMsg);
        Assert.assertEquals(err5.getText(), passErrMsg);
        Assert.assertEquals(err6.getText(), passConfirmErrMsg);

        driver.close();
    }

}
